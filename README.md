# Eazystyles AMCSS
Eazestyles - Lightweight AMCSS-based CSS framework with Emmet-like abbreviations. For fast markup without using css.
 
# Install

## NPM
`npm install eazystyles`

## Yarn
`yarn add eazystyles`

# Include Eazystyles to your project
You should include css-file in your project like this:

## React.js
`require('eazystyles');`

## Others
`<link rel="stylesheet" href="{path-to-eazestyles.css}">`

# How to use

## Flexbox
Firstly you should add **fl** attribute to your flex-container
to set it as `display: flex`

`<div fl>...</div>`
Then you can add some other attributes to manage flex properties

### Direction
`<div fl fl-d-rr>...</div>`
* **fl-d-rr** - `flex-direction: row-reverse`
* **fl-d-c** - `flex-direction: column`
* **fl-d-cr** -`flex-direction: column-reverse`

### Wrap
`<div fl fl-w>...</div>`
* **fl-w** - `flex-wrap: wrap`
* **fl-w-r** - `flex-wrap: wrap-reverse`

### Wrap
`<div fl fl-w>...</div>`
* **fl-w** - `flex-wrap: wrap`
* **fl-w-r** - `flex-wrap: wrap-reverse`

### Justify content
`<div fl fl-j-e>...</div>`
* **fl-j-e** - `justify-content: flex-end`
* **fl-j-c** - `justify-content: center`
* **fl-j-s** - `justify-content: space-between`
* **fl-j-s** - `justify-content: space-around`
* **fl-j-s** - `justify-content: space-evenly`

### Align items
`<div fl fl-ai-e>...</div>`
* **fl-ai-e** - `align-items: flex-end`
* **fl-ai-c** - `align-items: center`
* **fl-ai-b** - `align-items: baseline`
* **fl-ai-s** - `align-items: stretch`

### Align content
`<div fl fl-ac-e>...</div>`
* **fl-ac-e** - `align-content: flex-end`
* **fl-ac-c** - `align-content: center`
* **fl-ac-sb** - `align-content: space-between`
* **fl-ac-sa** - `align-content: space-around`
* **fl-ac-s** - `align-content: stretch`

### Align self
```
<div fl fl-ai-e>
  <div>...</div>
  <div fl-as-fs>...</div>
  <div>...</div>
</div>
```
* **fl-as-fs** - `align-self: flex-start`
* **fl-as-e** - `align-self: flex-end`
* **fl-as-c** - `align-self: center`
* **fl-as-b** - `align-self: baseline`
* **fl-as-s** - `align-self: stretch`

### Order
```
<div fl>
  <div fl-o-3>...</div>
  <div fl-o-1>...</div>
  <div fl-o-2>...</div>
</div>
```
* **fl-o-1** - `order: 1`
* **fl-o-2** - `order: 2`
* **fl-o-3** - `order: 3`
* **fl-o-4** - `order: 4`
* **fl-o-5** - `order: 5`
* **fl-o-6** - `order: 6`
* **fl-o-7** - `order: 7`
* **fl-o-8** - `order: 8`
* **fl-o-9** - `order: 9`
* **fl-o-10** - `order: 10`
* **fl-o-11** - `order: 11`
* **fl-o-12** - `order: 12`

### Grow
```
<div fl>
  <div fl-g-1>...</div>
  <div fl-g-2>...</div>
  <div fl-g-0>...</div>
</div>
```
* **fl-g-0** - `flex-grow: 0`
* **fl-g-1** - `flex-grow: 1`
* **fl-g-2** - `flex-grow: 2`

### Shrink
```
<div fl>
  <div fl-s-1>...</div>
  <div fl-s-2>...</div>
  <div fl-s-0>...</div>
</div>
```
* **fl-s-0** - `flex-shrink: 0`
* **fl-s-1** - `flex-shrink: 1`
* **fl-s-2** - `flex-shrink: 2`

## 12-column layout
```
<div fl>
  <div fl-col-6>...</div>
</div>
```
* **fl-col-1** - `flex-basis: 8.3333%`
* **fl-col-2** - `flex-basis: 16.6666%`
* **fl-col-3** - `flex-basis: 25%`
* **fl-col-4** - `flex-basis: 33.3333%`
* **fl-col-5** - `flex-basis: 41.6666%`
* **fl-col-6** - `flex-basis: 50%`
* **fl-col-7** - `flex-basis: 58.3333%`
* **fl-col-8** - `flex-basis: 66.6666%`
* **fl-col-9** - `flex-basis: 75%`
* **fl-col-10** - `flex-basis: 83.3333%`
* **fl-col-11** - `flex-basis: 91.6666%`
* **fl-col-12** - `flex-basis: 100%`

You can add rules for other monitor resolution by suffixes **sm**, **md**, **lg**, **xl**
* without suffix - from 0px
* **sm** - from 640px
* **md** - from 800px
* **lg** - from 1024px
* **xl** - from 1366px
```
<div fl>
  <div fl-col-12 fl-col-sm-6>...</div>
  <div fl-col-12 fl-col-sm-6>...</div>
  <div fl-col-12>...</div>
</div>
```

## Utilities
```
<ul>
  <li>...<li>
  <li unstyle><li>
  <li>...<li>
</ul>
```
```
<ol unstyle>
  <li>...<li>
  <li>...<li>
  <li>...<li>
</ol>
```
* **unstyle** - `list-style:none`

`<p ta-r>Hello world!</p>`
* **ta-l** - `text-align: left`
* **ta-c** - `text-align: center`
* **ta-r** - `text-align: right`
* **ta-j** - `text-align: justify`
